const http = require('http');
const qs = require('querystring');

const server = http.createServer((req, res) => {
    if (req.method === 'GET') {
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.end(`
            <!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Registration Form</title>
            </head>
            <body>
                <h2>Registration Form</h2>
                <form action="/" method="POST">
                    <label for="username">Username:</label><br>
                    <input type="text" id="username" name="username" required><br><br>
                    
                    <label for="email">Email:</label><br>
                    <input type="email" id="email" name="email" required><br><br>
                    
                    <label for="password">Password:</label><br>
                    <input type="password" id="password" name="password" required><br><br>
                    
                    <input type="submit" value="Register">
                </form>
            </body>
            </html>
        `);
    } else if (req.method === 'POST') {
        let body = '';
        req.on('data', (chunk) => {
            body += chunk.toString();
        });

        req.on('end', () => {
            const formData = qs.parse(body);
            const { username, email, password } = formData;
            
            // For simplicity, just logging the received data
            console.log(`Received registration request: Username - ${username}, Email - ${email}, Password - ${password}`);
            
            res.writeHead(200, { 'Content-Type': 'text/html' });
            res.end('Registration successful!');
        });
    }
});

const PORT = process.env.PORT || 3000;

server.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
